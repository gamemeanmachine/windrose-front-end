﻿using GMM.Utils;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


namespace BackPack
{
	namespace Behaviours
	{
		namespace UI
		{
			namespace Inventory
			{
				namespace Basic
				{
					[RequireComponent(typeof(Button))]
					public class BasicSingleSimpleInventoryViewNextButton : MonoBehaviour {
						/**
					     * The parent SimpleInventoryView object installs a handler for
					     *   onClick event. This behaviour is just a marker/profiler for
					     *   the parent to correctly identify the button to install
					     *   the handler.
					     */
					}
				}
			}
		}
	}
}
